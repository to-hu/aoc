#lang racket/base

(require "main.rkt")

(module+ test
  (require rackunit rackunit/text-ui)

  (define suite
    (test-suite
     "fuel tests"
     (test-eq? "first simple example"
               (fuel 12)
               2)
     (test-eq? "another simple example"
               (fuel 14)
               2)
     (test-eq? "bigger"
               (fuel 1969)
               654)
     (test-eq? "even bigger"
               (fuel 100756)
               33583)
     (test-eq? "first total fuel example"
               (total-fuel 14)
               2)
     (test-eq? "second total fuel example"
               (total-fuel 100756)
               50346)
     (test-eq? "third total fuel example"
               (total-fuel 1969)
               966)))
  (run-tests suite))
