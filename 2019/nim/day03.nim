import strutils
import wires

type
  Orientation = enum vertical, horizontal
  Point = tuple[x: int, y: int]
  Section = tuple[startPoint, endPoint: Point, orientation: Orientation]
  System = tuple[sizeX: int, sizeY: int, origin: Point]


proc getOrientiaron(startPoint, endPoint:Point):Orientation
proc hasIntersectionPoint(sec1, sec2: Section):bool
proc isInBetween(value: int, section: Section): bool
proc calcIntersectionPoint(sec1, sec2: Section):Point
# proc buildWirePathList(wirePath: seq[string]): seq[Point]
proc interprete(instruction: string): Point
proc add(p1: var Point, p2: Point)
proc getSystemSize(wirepath1, wirepath2: array[0..300, string]): System
proc buildGrids(wirepath1, wirepath2: array[0..300, string], system: System): seq[Point]
proc execute(instruction: string, currentPos: Point, grid: var seq[seq[int]]): seq[Point]
proc mark(grid: var seq[seq[int]], position: Point): int 
proc step(position: var Point, direction: char, distance: int): int = 


proc buildGrids(wirepath1, wirepath2: array[0..300, string], system: System): seq[Point] =
  var
    grid: seq[seq[int]]
    position = system.origin
    newPosition: Point
  grid.setLen(system.sizeX)
  for index in 0..<grid.len:
    grid[index].setLen(system.sizeY)
  for instruction in wirepath1.items:
    newPosition = instruction.interprete
    for 
    
    
proc execute(instruction: string, currentPos: Point, grid: var seq[seq[int]]): seq[Point] =
  let
    direction = instruction[0]
  var
    pos = currentPos
    distance = parseInt(instruction[1..^1])

  while distance != 0:
    distance = pos.step(direction, distance)
    if grid.mark(pos) == -1:
      result.add(pos)
    

proc step(position: var Point, direction: char, distance: int): int = 
  result = distance
  case direction
  of 'R':
    position.x += 1
    result -= 1
  of 'L':
    position.x -= 1
    result += 1
  of 'D':
    position.y -= 1
    result += 1
  of 'U':
    position.y += 1
    result -= 1
  else:
    discard



proc mark(grid: var seq[seq[int]], position: Point): int =
  let
    x = position.x
    y = position.y
  case grid[x][y]
  of 0:
    grid[x][y] = 1
    return -1
  of 1:
    grid[x][y] = -1
    return -1
  else:
    grid[x][y] = -1
    return 0
  



proc getSystemSize(wirepath1, wirepath2: array[0..300, string]): System =
  let
    wirepaths = @[wirepath1, wirepath2]
  var
    xs, ys: seq[int]
    position = (x: 0, y: 0)
    sizeX, sizeY: int
    origin: Point
  for wirepath in wirepaths.items:
    for instruction in wirepath.items:
      position.add(interprete(instruction))
      xs.add(position.x)
      ys.add(position.y)
  sizeX = abs(min(xs)) + abs(max(xs)) + 1
  sizeY = abs(min(ys)) + abs(max(ys)) + 1
  origin = (x: abs(min(xs)), y: abs(min(ys)))
  result = (sizeX: sizeX, sizeY: sizeY, origin: origin)

      

proc getOrientiaron(startPoint, endPoint:Point): Orientation =
  if startPoint.x == endPoint.x:
    return vertical
  elif startPoint.y == endPoint.y:
    return horizontal
  else:
    echo "Error in getOrientiaron. Section is not horizontal neither veritcal"


proc hasIntersectionPoint(sec1, sec2: Section): bool = 
  if sec1.orientation == sec2.orientation:
    return false
  let
    verticalSection = if sec1.orientation == vertical: sec1
                      else: sec2
    horizontalSection = if sec1.orientation == horizontal: sec1
                        else: sec2
    verticalX = verticalSection.startPoint.x
    horizontalY = horizontalSection.startPoint.y

  result = verticalX.isInBetween(horizontalSection)
  result = result and horizontalY.isInBetween(verticalSection)


proc isInBetween(value: int, section: Section): bool =
  let
    lowerBound = if section.orientation == vertical: section.startPoint.y
                 else: section.startPoint.x
    upperBound = if section.orientation == vertical: section.endPoint.y
                 else: section.endPoint.x
  return lowerBound < value and value < upperBound


proc calcIntersectionPoint(sec1, sec2: Section): Point =
  if sec1.orientation == vertical and sec2.orientation == horizontal:
    result = (x: sec1.endPoint.x, y: sec2.endPoint.y)
  elif sec1.orientation == horizontal and sec2.orientation == vertical:
    result = (x: sec2.endPoint.x, y: sec1.endPoint.y)


proc interprete(instruction: string): Point =
  let
    direction = instruction[0]
    distance = parseInt(instruction[1..^1])
  case direction
  of 'R':
    return (x: distance, y: 0)
  of 'L':
    return (x: -distance, y: 0)
  of 'D':
    return (x: 0, y: -distance)
  of 'U':
    return (x: 0, y: distance)
  else:
    discard
      


proc add(p1: var Point, p2: Point) =
  p1.x = p2.x
  p1.y = p2.y


echo $getSystemSize(wire1, wire2)
