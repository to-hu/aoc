import strutils, sequtils

var
  f:File
  lst:seq[int]
if f.open("../input/day01.txt"):
  for line in f.lines:
    lst.add(parseInt(line))

proc calcFuelRecursive(mass: int): int =
  result = (mass div 3) - 2
  if result < 1:
    result = 0
  else:
    result += calcFuelRecursive(result)

proc calcFuel(mass: int): int =
  result = (mass div 3) - 2
  if result < 1:
    result = 0
  
echo "Day1 first part: " & $foldl(lst.map(calcFuel), a + b)
echo "Day1 second part: " & $foldl(lst.map(calcFuelRecursive), a + b)
