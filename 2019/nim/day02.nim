import sequtils
let input = @[1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,5,19,23,1,23,5,27,1,
              27,13,31,1,31,5,35,1,9,35,39,2,13,39,43,1,43,10,47,1,47,13,51,2,
              10,51,55,1,55,5,59,1,59,5,63,1,63,13,67,1,13,67,71,1,71,10,75,1,
              6,75,79,1,6,79,83,2,10,83,87,1,87,5,91,1,5,91,95,2,95,10,99,1,9,
              99,103,1,103,13,107,2,10,107,111,2,13,111,115,1,6,115,119,1,119,
              10,123,2,9,123,127,2,127,9,131,1,131,10,135,1,135,2,139,1,10,139,
              0,99,2,0,14,0]


proc runOptCode(input: seq[int]): seq[int]
proc getPosition(input: seq[int], address:int): int
proc writePosition(input: var seq[int], address:int, value:int)
proc output(optcode: seq[int]): int 
proc setNounVerb(noun, verb: int): seq[int] 


proc runOptCode(input: seq[int]): seq[int]=
  var
    index, src1, src2, value = 0
  result.setLen(input.len)
  for i, item in input.pairs:
    result[i] = item
  while result[index] != 99:
    src1 = result.getPosition(index + 1)
    src2 = result.getPosition(index + 2)
    value = if result[index] == 1: src1 + src2 else: src1 * src2
    result.writePosition(index + 3, value)
    index += 4
    
proc getPosition(input: seq[int], address:int): int=
  return input[input[address]]


proc writePosition(input: var seq[int], address:int, value:int)=
  input[input[address]] = value


proc findVerbNone(output: int): tuple[noun: int, verb: int] =
  for noun in countup(0, 99):
    for verb in countup(0, 99):
      if output(setNounVerb(noun, verb)) == output:
        return (noun, verb)
      

proc setNounVerb(noun, verb: int): seq[int] =
  result.setLen(input.len)
  for n, item in input.pairs:
    if n == 1:
      result[n] = noun
    elif n == 2:
      result[n] = verb
    else:
      result[n] = item


proc output(optcode: seq[int]): int =
  return runOptCode(optcode)[0]
  


echo "Solution Part 1: " & $runOptCode(input)[0]
echo "Solution Part 2: " & $findVerbNone(19690720)




  
assert(runOptCode(@[1,0,0,0,99]) == @[2,0,0,0,99])
assert(runOptCode(@[2,3,0,3,99]) == @[2,3,0,6,99])
assert(runOptCode(@[2,4,4,5,99,0]) == @[2,4,4,5,99,9801])
assert(runOptCode(@[1,1,1,4,99,5,6,0,99]) == @[30,1,1,4,2,5,6,0,99])
assert(runOptCode(@[1,9,10,3,2,3,11,0,99,30,40,50]) == @[3500,9,10,70,2,3,11,0,99,30,40,50])
