(ns aoc
  (:require [clojure.java.io :as io]))


(defn read-lines [year day]
  (->> (str year "/day" day ".txt")
       io/resource
       io/reader
       line-seq))

(defn two-part-input [year day]
  (let [input (read-lines year day)
        empty-line? #(= "" %)]
    [(vec (take-while empty-line? input))
     (vec (rest (drop-while empty-line? input)))]))

(defn two-part-input [year day]
  (let [input (read-lines year day)
        two-parts (split-at  (count (take-while #(not= "" %) input))
                             input)]
    [(vec (first two-parts))
     (vec (rest (second two-parts)))]))

(defn input-line [year day]
  (first (aoc/read-lines year day)))
