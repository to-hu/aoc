(ns clojure-solution.day20-test
  (:require [clojure-solution.day20 :as sut]
            [clojure.test :refer :all]
            [clojure-solution.aoc :as aoc]))



(def image-enhancement-algo (sut/->image-enhancement-algo "day20_example.txt"))
(def image (sut/->image "day20_example.txt"))


(deftest day20
  (testing "part1"
    (is (= \# (aoc/apply-kernel (sut/widen image) sut/kernel [4 4] sut/combine-f)))
    (is (= 35 (sut/solve1 image image-enhancement-algo)))))


#_(map println (sut/enhance-times image image-enhancement-algo))
