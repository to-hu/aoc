(ns clojure-solution.day15-test
  (:require [clojure-solution.day15 :as sut]
            [clojure.test :refer :all]
            [clojure-solution.aoc :as aoc]))

(def risk-level-matrix (sut/->risk-level-matrix "day15_example.txt"))


(deftest day15
  (testing "part1"
    (is (= [9 9] (sut/end-point risk-level-matrix)))
    (is (= 40 (sut/dijkstra risk-level-matrix)))
    (is (= (map #(- (int %) 48) "11637517422274862853338597396444961841755517295286")
           (first (sut/enlarge risk-level-matrix))))
    (is (= [[8 9 1 2 3]
            [9 1 2 3 4]
            [1 2 3 4 5]
            [2 3 4 5 6]
            [3 4 5 6 7]]
         (sut/enlarge [[8]])))
    (is (= 315 (sut/dijkstra (sut/enlarge risk-level-matrix))))
    ))


#_((sut/->node-map risk-level-matrix) [1 1])
