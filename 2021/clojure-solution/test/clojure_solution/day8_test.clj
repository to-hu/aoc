(ns clojure-solution.day8-test
  (:require [clojure-solution.day8 :as sut]
            [clojure.test :refer :all]
            [clojure.java.io :as io]
            [clojure.string :as string]))


(def lines (->> "day8_example.txt"
                io/resource
                io/reader
                line-seq))

(deftest day8
  (testing "part1"
    (is (= 26 (sut/solve1 lines)))))

(deftest day8
  (testing "part2"
    (is (= {"acedgfb" 8 "cdfbe" 5 "gcdfa" 2 "fbcad" 3 "dab" 7 "cefabd" 9 "cdfgeb" 6 "eafb" 4 "cagedb" 0 "ab" 1}
           (sut/deduce-pattern "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")))
    (is (= 61229 (sut/solve2 lines)))))
#_(sut/analyze (first (map sut/parse lines)))
