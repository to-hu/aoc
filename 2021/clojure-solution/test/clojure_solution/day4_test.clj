(ns clojure-solution.day4-test
  (:require [clojure-solution.day4 :as sut]
            [clojure.java.io :as io]
            [clojure.test :refer :all]))


(def lines (line-seq (io/reader (io/resource "day4_test.txt"))))


(def draw-stack (read-string (str "[" (first lines) "]")))

(def draw-hashmap (zipmap draw-stack (range)))

(def boards (->> (rest lines)
                 (filter #(not= % ""))
                 (map #(str "[" % "]"))
                 (map read-string)
                 (partition 5)
                 (map flatten)))


(deftest day4-part1
  (let [[draw-stack boards] (sut/parse lines)
        draw-stack-index-winning-draw (sut/get-earliest-bingo  draw-stack (nth boards 2))]
    (is (=  24 (nth draw-stack draw-stack-index-winning-draw)))
    (is (= '(7 4 9 5 11 17 23 2 0 14 21 24) (take (inc draw-stack-index-winning-draw) draw-stack)))
    (is (= 4512 (sut/solve1 lines)))
    ))


(deftest day4-part2
  (let [[draw-stack boards] (sut/parse lines)
        draw-stack-index-loosing (sut/get-latest-bingo draw-stack (nth boards 1))]
    (is (= 14 (nth draw-stack draw-stack-index-loosing)))))
