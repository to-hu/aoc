(ns clojure-solution.day5-test
  (:require [clojure-solution.day5 :as sut]
            [clojure.java.io :as io]
            [clojure.test :refer :all]))


(def lines (line-seq (io/reader (io/resource "day5_example.txt"))))

(deftest part1
  (is (= 5 (sut/solve1 lines )))
  (is (= 12 (sut/solve2 lines)))
  (is (= '([1 1] [1 2] [1 3]) (sut/expand [1 1 1 3])))
  (is (= '([9 7] [8 7] [7 7]) (sut/expand [9 7 7 7])))
  (is (= '([1 1] [2 2] [3 3]) (sut/expand [1 1 3 3])))
  (is (= '([9 7] [8 8] [7 9]) (sut/expand [9 7 7 9]))))
