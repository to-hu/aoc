(ns clojure-solution.day6-test
  (:require [clojure-solution.day6 :as sut]
            [clojure.test :refer :all]))

(def initial-state [3,4,3,1,2])


(deftest day6
  (testing "part1"
    (is (= 26 (sut/solve1 initial-state 18))))
  (testing "part2"
    (is (= 26984457539 (sut/solve3 initial-state 256))
        (= (sort (sut/solve1 initial-state 16))
           (sort (sut/solve3 initial-state 16))))
    (is (= 26984457539 (sut/evo-256-days initial-state)))))
