(ns clojure-solution.day11-test
  (:require [clojure-solution.day11 :as sut]
            [clojure.test :refer :all]
            [clojure-solution.aoc :as aoc]))

(def small-example (mapv sut/parse (aoc/read-lines "day11_small_example.txt")))
(def example (mapv sut/parse (aoc/read-lines "day11_example.txt")))


(deftest day11
  (testing "part1"
    (is (= 9 (first (sut/step small-example ))))
    (is (= 1656 (sut/solve1 example))))
  (testing "part2"
    (is (= 195 (sut/solve2 example)))))
