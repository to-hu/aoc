(ns clojure-solution.day10-test
  (:require [clojure-solution.day10 :as sut]
            [clojure.test :refer :all]
            [clojure-solution.aoc :as aoc]))


(def incomplete-example "[({(<(())[]>[[{[]{<()<>>")
(def corrupted-example "{([(<{}[<>[]}>{[]{[(<()>")
(def lines (aoc/read-lines "day10_example.txt"))


(deftest day10
  (testing "part1"
    (is (= \} (sut/corrupted? corrupted-example)))
    (is (= 26397 (sut/solve1 lines))))
  (testing "part2"
    (is (= '(\} \} \] \] \) \} \) \]) (sut/complete (sut/corrupted? incomplete-example))))
    (is (= 294 (sut/calc-score '(\] \) \} \>))))
    (is (= 288957 (sut/solve2 lines)))))
