(ns clojure-solution.day16-test
  (:require [clojure-solution.day16 :as sut]
            [clojure.test :refer :all]))

(defn bitstring->bitstream [s]
  (->> s
       vec
       (mapv str)
       (mapv #(Integer/parseInt %))))

(deftest day16
  (testing "part1"
    (is (= 4 (sut/bin->dec [1 0 0])))
    (are [x y] (= x (sut/bin->dec (sut/hex-bin-m y)))
      0 \0
      1 \1
      2 \2
      3 \3
      4 \4
      5 \5
      6 \6
      7 \7
      8 \8
      9 \9
      10 \A
      11 \B
      12 \C
      13 \D
      14 \E
      15 \F)
    (is (= [[6 4 2021]] (sut/parse (bitstring->bitstream "110100101111111000101000"))))
    ))
