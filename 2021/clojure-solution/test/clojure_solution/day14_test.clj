(ns clojure-solution.day14-test
  (:require [clojure-solution.day14 :as sut]
            [clojure.test :refer :all]
            [clojure-solution.aoc :as aoc]))



(def lines (aoc/read-lines "day14_example.txt"))
(def insertion-rules (sut/->pair-insertion-rules lines))
(def template (sut/->template lines))


(deftest day14
  (testing "part1"
    (is (= [:N :C :N :B :C :H :B] (sut/step insertion-rules template)))
    (is (= 1588 (sut/solve1 lines)))))
