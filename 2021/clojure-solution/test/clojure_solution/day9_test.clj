(ns clojure-solution.day9-test
  (:require  [clojure.test :refer :all]
             [clojure-solution.aoc :as aoc]
             [clojure-solution.day9 :as sut]))

(def lines (aoc/read-lines "day9_example.txt"))
(def height-map (sut/->height-map lines))

(deftest day1
  (testing "part1"
    (are [x y](= (sort x) (sort (aoc/apply-kernel height-map sut/kernel-part1 y)))
      [9 9 3] [5 0]
      [2 9 9] [1 0]
      [9 8 8 6] [3 1])
    (are [x y] (= x (sut/low-point? height-map y))
      [[1 0] 1] [1 0]
      false [1 1]
      [[2 2] 5] [2 2]
      false [5 0])
    (is (= 15 (sut/solve1 lines)))))


(deftest day2
  (testing "part2"
    (are [x y] (= x (count (sut/region-growth height-map y)))
      3 [0 0])
    (is (= 1134 (sut/solve2 lines)))))
