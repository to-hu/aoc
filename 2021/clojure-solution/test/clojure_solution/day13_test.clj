(ns clojure-solution.day13-test
  (:require [clojure-solution.day13 :as sut]
            [clojure.test :refer :all]
            [clojure-solution.aoc :as aoc]))


(def lines (aoc/read-lines "day13_example.txt"))
(def paper (sut/->paper (sut/->dots lines)))
(def fold-instructions (sut/->folds lines))


(deftest day13
  (testing "part1"
    (is (= 17 (sut/count-dots (sut/horizontal-fold paper 7))))
    (is (= 17 (sut/solve1 lines)))))
