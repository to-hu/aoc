(ns clojure-solution.day15
  (:require [clojure-solution.aoc :as aoc]
            [clojure.data.priority-map :refer [priority-map]]))

(defn parse [lines]
  (mapv #(->> %
              (apply list)
              (mapv str)
              (mapv (fn [line]
                      (Integer/parseInt line))))
        lines))

(defn ->risk-level-matrix [resource]
  (parse (aoc/read-lines resource)))


(defn enlarge [risk-level-matrix]
  (letfn [(enlarge-line [line]
            (reduce (fn [acc risk-increase]
                      (into acc (mapv #(let [new-risk (+ risk-increase %)]
                                         (if (> new-risk 9)
                                           (mod new-risk 9)
                                           new-risk))
                                      line)))
                    []
                    (range 10)))]
    (let [original-width (count (first risk-level-matrix))
          increaded-risk-level-matrixes (reduce #(conj %1 (enlarge-line %2))
                                                []
                                                risk-level-matrix)]
      (reduce (fn [acc n]
                (->> increaded-risk-level-matrixes
                     (map (partial drop (* n original-width)))
                     (map (partial take (* 5 original-width)))
                     (map vec)
                     (into acc)))
              []
              (range 5)))))

(def risk-level-matrix (->risk-level-matrix "day15.txt"))



(defn get-number-nodes [risk-level-matrix]
  (apply + (map count risk-level-matrix)))

(defn get-number-edges [risk-level-matrix]
  (let [x-edge-len (- (count (first risk-level-matrix)) 2)
        y-edge-len (- (count risk-level-matrix) 2)
        corners (* 4 2)
        edges (* (+ x-edge-len
                    y-edge-len)
                 3)
        inner (* x-edge-len y-edge-len 4)]
    (+ corners edges inner)))

(def n-v (get-number-nodes risk-level-matrix))
(def n-e (get-number-edges risk-level-matrix))

(defn method-graph-in-memory[risk-level-matrix]
  (let [number-edges (get-number-edges risk-level-matrix)
        number-nodes (get-number-nodes risk-level-matrix)
        methods [[:adjazenz-matrix (* number-edges number-edges)]
                 [:edge-list (* 2 (* 2 number-edges))]
                 [:node-list(+ 2 number-edges number-nodes)]]]
    (->> methods
         (sort-by second)
         )))

(defn flatten-once [x]
  (reduce #(apply conj %1 %2) [] x))

(defn ->graph [risk-level-matrix]
  "returns a map of coordinates associated with a map of neighbour coordinates with weight "
  (let [nodes (aoc/all-coordinates-of risk-level-matrix)
        x-max (count (first risk-level-matrix))
        node-matrix (partition x-max nodes)
        combined (mapv (fn [x y]
                         (mapv #(vector %1 %2) x y))
                       node-matrix
                       risk-level-matrix)
        none? (constantly false)
        kernel [none?    identity none?
                identity none?    identity
                none?    identity none?]]
    (reduce (fn [acc coordinates]
              (let [outgoing-edges (->> coordinates
                                        (aoc/apply-kernel combined kernel)
                                        flatten-once
                                        (apply hash-map))]
                (assoc acc coordinates outgoing-edges)))
            {}
            nodes)))

(defn end-point [risk-level-matrix]
  (map dec [(count (first risk-level-matrix))
            (count risk-level-matrix)]))

(def inf Long/MAX_VALUE)


(defn update-risks [risk-sum risks neighbours]
  (reduce-kv (fn [acc coordinate risk]
               (let [new-risk (+ risk-sum risk)]
                 (if (> (acc coordinate)
                             new-risk)
                   (assoc acc coordinate new-risk)
                   acc)))
             risks
             neighbours))


(defn dijkstra
  ([risk-level-matrix] (dijkstra risk-level-matrix [0 0] (end-point risk-level-matrix)))
  ([risk-level-matrix start end]
   (let [graph (->graph risk-level-matrix)
         unvisited (set (keys graph))
         risks (apply conj (priority-map) (zipmap (keys graph) (repeat inf)))
         risks (assoc risks start 0)]
     (loop [risks risks unvisited unvisited]
       (let [[most-harmless-node risk]  (some (fn [[node :as all]] (when (contains? unvisited node) all)) risks)]
         (if (= end most-harmless-node)
           risk
           (recur (update-risks risk
                               risks
                               (graph most-harmless-node))
                  (disj unvisited most-harmless-node))))))))

#_(dijkstra (enlarge risk-level-matrix))
