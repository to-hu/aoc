(ns clojure-solution.day5
  (:require [clojure.java.io :as io]
            [clojure.string :as string]))

(def lines (line-seq (io/reader (io/resource "day5.txt"))))

(defn parse [line]
  (str "["(string/replace line "->" "")"]"))

(defn correct-direction [z1 z2]
  (if (> z2 z1) [z1 z2] [z2 z1]))

(defn expand [[x1 y1 x2 y2]]
  (let [
        ;; [x1 x2] (correct-direction x1 x2)
        ;; [y1 y2] (correct-direction y1 y2)
        max-distance (inc (max (Math/abs (- x2 x1))
                               (Math/abs (- y2 y1))))]
    (letfn [(generate [z1 z2]
              (let [[f step ] (if (< z1 z2) [inc  1] [dec -1 ])]
                (take max-distance (cycle (range z1 (f z2) step)))))]
      (map vector
           (generate x1 x2)
           (generate y1 y2)))))

(defn solve1 [lines]
  (->> lines
       (map parse)
       (map read-string)
       (filter (fn [[x1 y1 x2 y2]] (or (= x1 x2)
                                       (= y1 y2))))
       (map expand)
       (apply concat)
       frequencies
       (filter (fn [[_ n-times]]
                 (and (> n-times 1))))
       count))

(defn solve2 [lines]
  (->> lines
       (map parse)
       (map read-string)
       (map expand)
       (apply concat)
       frequencies
       (filter (fn [[_ n-times]]
                 (and (> n-times 1))))
       count))
