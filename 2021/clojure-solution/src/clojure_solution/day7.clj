(ns clojure-solution.day7
  (:require [clojure.java.io :as io]))


(def horizontal-positions (read-string (str "["(slurp (io/resource "day7.txt"))"]")))


(defn get-fuels [align-position horizontal-positions]
  (map #(if (> % align-position)
          (- % align-position)
          (- align-position %))
       horizontal-positions))


(defn get-fuels2 [align-position horizontal-positions]
  (map #(apply + (range 0 (inc %)) (get-fuels align-position horizontal-positions))))

(defn get-height [get-fuels horizontal-positions]
  (let [minima (apply min horizontal-positions)
        maxima (apply max horizontal-positions)
        fuel-per-height (map #(apply + (get-fuels % horizontal-positions))
                    (range minima (inc maxima)))]
    (apply min fuel-per-height)))


#_(->> horizontal-positions
       (get-height get-fuels) )


#_(->> horizontal-positions
       (get-height get-fuels2))
