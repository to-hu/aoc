(ns clojure-solution.aoc
  (:require [clojure.java.io :as io]))

(defn read-lines [resource]
  (->> resource
       io/resource
       io/reader
       line-seq))

(defn first-line&rest [resource]
  (let [[first _ & rest] (read-lines resource)]
    [first rest]))

(defn bin->dec [bits]
  (reduce (fn [acc [b n]]
            (+ acc (* b (int (Math/pow 2 n)))))
          0
          (map vector (reverse bits)
               (range))))

(defn all-coordinates-of [matrix]
  (for [x (range (count matrix))
        y (range (count (first matrix)))]
    [x y]))

(defn get-kernel-coordinates [matrix kernel [x y]]
  (let [y-max (count (first matrix))
        x-max (count matrix)
        offset (int (/ (Math/sqrt (count kernel))
                       2))]
    (map (fn [[x y :as coordinate]]
           (and (>= x 0)
                (>= y 0)
                (< x x-max)
                (< y y-max)
                coordinate))
         (for [k (range (- x offset) (inc (+ x offset)))
               m (range (- y offset) (inc (+ y offset)))]
           [k m]))))

(defn apply-kernel
  ([matrix kernel [x y :as coordinate]]
   (apply-kernel matrix kernel coordinate #(apply vector %)))
  ([matrix kernel [x y :as coordinate] combine-f]
   (let [hm-coordinates (get-kernel-coordinates matrix kernel coordinate)
         hm-values (map #(and % (get-in matrix %)) hm-coordinates)
         filter-f&hm-values (map vector kernel
                                 hm-values)
         filter-results (map (fn [[f x]](f x)) filter-f&hm-values)]
     (combine-f (filter identity filter-results)))))

(defn transpose [matrix]
  (apply mapv vector matrix))
