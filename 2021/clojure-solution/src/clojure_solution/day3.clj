(ns clojure-solution.day3
  (:require
   [clojure.java.io :as io]))


(def lines (line-seq (io/reader (io/resource "day3.txt"))))

(defn transpose [matrix]
  (apply mapv vector matrix))


(defn most-commons [frequencies]
  (if (= (count frequencies) 1)
    (apply str (keys frequencies))
    (let [zero (get frequencies \0)
        one (get frequencies \1)]
    (if (> zero one)
      0
      1))))

(defn powof2 []
  (map #(int (Math/pow 2 %))
       (range)))


(defn bin->dec [bin]
  (->> bin
       reverse
       (map vector (powof2))
       (map (partial apply *))
       (apply +)))

(defn gamma [lines]
  (->> lines
       transpose
       (map frequencies)
       (map most-commons)))

(defn epsilon [lines]
  (map #(if (= % 0) 1 0) (gamma lines)))

#_(->> [(epsilon lines) (gamma lines)]
       (map bin->dec)
       (apply *)
       )

(defn rating [f lines]
  (let [line-length (count (first lines))]
    (reduce (fn [acc index]
              (let [bit-criteria (if (= (count acc) 1)
                                   (subs (first acc) index (inc index))
                                   (-> acc
                                     f
                                     (nth index)
                                     str))]
                (filter (fn [sample]
                          (= (subs sample index (inc index))
                            bit-criteria)) acc)))
            lines
            (range line-length))))

#_(->> lines
       (#(vector (rating gamma %) (rating epsilon %)))
       (map first)
       (map (partial apply list))
       (map (partial map (comp #(Integer/parseInt %) str)))
       (map bin->dec)
       (apply *)
  )
