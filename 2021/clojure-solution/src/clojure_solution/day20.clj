(ns clojure-solution.day20
  (:require [clojure-solution.aoc :as aoc]))


(defn ->image-enhancement-algo [resource]
  (apply vector (first (aoc/first-line&rest resource))))

(defn widen [image]
  (let [width (count (first image))
        line (vec (repeat width \.))
        rows-added `[~line ~@image ~line]]
    (mapv (fn [line] `[\. ~@line \.]) rows-added)))


(defn ->image [resource]
  (widen (second (aoc/first-line&rest resource))))

(def enhancement-algo (->image-enhancement-algo "day20.txt"))
(def image (->image "day20.txt"))
(def kernel (repeat 9 #(if (= \# %) 1 0)))

(defn apply-algo
  ([image algo] (apply-algo image algo kernel))
  ([image algo kernel]
   (let [combine-f #(nth algo (aoc/bin->dec %))
         widened-image (widen image)
        width (count (first widened-image))
        height (count widened-image)]
    (reduce (fn [acc pixel]
              (assoc-in acc pixel (aoc/apply-kernel widened-image kernel pixel combine-f)))
            widened-image
            (for [y (range 1 (dec height))
                  x (range 1 (dec width))]
              [y x])))))


(defn ligth-pixels [image]
  (->> image
       (map (fn [row] (count (filter (partial = \#) row))))
       (apply +)))


(defn enhance-times [image algo n]
  (reduce (fn [acc _]
            (apply-algo acc algo))
          image
          (range n)))


(defn solve1 [image enhancement-algo]
  (-> image
      (enhance-times enhancement-algo 2)
      ligth-pixels))
