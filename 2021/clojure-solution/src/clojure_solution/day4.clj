(ns clojure-solution.day4
  (:require [clojure.java.io :as io]))


(def lines (line-seq (io/reader (io/resource "day4.txt"))))

(defn parse [lines]
  [(read-string (str "[" (first lines) "]"))
   (->> (rest lines)
              (filter #(not= % ""))
              (map #(str "[" % "]"))
              (map read-string)
              (partition 5)
              (map flatten))])


(defn draw-hashmap [draw-stack]
  (zipmap draw-stack (range)))

(defn get-side-length [board]
  (->> board
       count
       Math/sqrt
       int))


(defn get-draw-times [draw-stack board]
  (map (partial get (draw-hashmap draw-stack)) board))

(defn get-earliest-bingo  [draw-stack board]
  (let [draw-times (get-draw-times draw-stack board)
        side-length (get-side-length board)
        rows (partition side-length draw-times)
        columns (apply map vector rows)
        rows&columns (concat rows columns)]
    (->> rows&columns
         (map (partial apply max))
         (apply min))))



(defn get-winning [strategy earliest-bingo-draws things]
  (let [min-number-draws (apply strategy earliest-bingo-draws)]
    (->> (map vector earliest-bingo-draws things)
         ((partial drop-while (fn [[bingo-draw]] (not= bingo-draw min-number-draws))))
         first
         second)))


(defn make-solver [strategy](fn[lines]
  (let [[draw-stack boards] (parse lines)
        earliest-bingo-draws (map (partial get-earliest-bingo draw-stack) boards)
        index-winning-number (apply strategy earliest-bingo-draws)
        winning-draw-number (nth draw-stack index-winning-number)
        marked-numbers (take (inc index-winning-number) draw-stack)
        winning-board (get-winning strategy earliest-bingo-draws boards)
        unmarked-numbers-of-board (apply disj (apply hash-set winning-board) marked-numbers)]
    (* winning-draw-number (apply + unmarked-numbers-of-board)))))

(def solve1 (make-solver min))
(def solve2 (make-solver max))

#_(->> lines
       solve1)

#_(->> lines
       solve2)
