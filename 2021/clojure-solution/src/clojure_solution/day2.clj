(ns clojure-solution.day2
  (:require
   [clojure.java.io :as io]))

(def lines (line-seq (io/reader (io/resource "day2.txt"))))

(defn parse [lines]
  (->> lines
       (map #(str "[" % "]"))
       (map read-string)))

(defn move [[horizontal depth] [direction n]]
  (condp = direction
    'forward [(+ n horizontal) depth]
    'up [horizontal (- depth n)]
    'down [horizontal (+ depth n)]))


(defn solve [instructions]
  (reduce (fn [position [f n]]
            (move f n position)) [0 0]
          instructions))



(defn move2 [[horizontal depth aim][direction n] ]
  (condp = direction
    'forward [(+ n horizontal) (+ depth (* aim n)) aim]
    'up [horizontal depth (- aim n)]
    'down [horizontal depth (+ aim n)]))


#_(->> lines
       parse
       (reduce move [0 0])
       (apply *))


#_(->> lines
       parse
       (reduce move2 [0 0 0])
       butlast
       (apply *))
