(ns clojure-solution.day13
  (:require [clojure-solution.aoc :as aoc]
            [clojure.string :as string]))

(def input (aoc/read-lines "day13.txt"))

(defn ->dots [input]
  (->> input
       (take-while #(not= "" %))
       (map #(str "[" % "]"))
       (mapv read-string)))

(defn ->folds [input]
  (->> input
       (drop-while #(not= "" %))
       rest
       (map #(string/replace %
                             #"fold along (.)=(\d+)"
                             (fn [[_ d n]] (str "[" d " " n "]"))))
       (map read-string)))

(defn ->paper [dots]
  (let [x-max (apply max (map first dots))
        y-max (apply max (map second dots))
        init-paper (mapv vec (take (inc y-max) (repeat (take (inc x-max) (repeat \.))))) ]
    (reduce (fn [acc coordinate]
              (update-in acc (reverse coordinate) (fn [_] \#)))
            init-paper
            dots)))

(defn horizontal-fold [matrix line-n]
  (aoc/transpose(vertical-fold (aoc/transpose matrix) line-n)))

(defn vertical-fold [matrix column-n]
  (let [stays (map (partial take column-n) matrix)
        fold (map (partial drop (inc column-n)) matrix)]
    (mapv (fn [s f]
            (mapv (fn [& args]
                    (if (some #(= \# %) args)
                      \#
                      \.))
                  s
                  (reverse f)))
          stays
          fold)))

(defn fold-paper [matrix [kind n]]
  (condp = kind
    'x (vertical-fold matrix n)
    'y (horizontal-fold matrix n)))

(defn count-dots [matrix]
  (count (filter #(= % \#) (flatten matrix))))

(defn solve1 [input]
  (-> input
      ->dots
      ->paper
      (fold-paper (first (->folds input)))
      count-dots))

(defn solve2 [input]
  (let [paper (->> input ->dots ->paper)
        folds (->> input ->folds)]
    (reduce fold-paper paper folds)))

#_(map println (solve2 input))
