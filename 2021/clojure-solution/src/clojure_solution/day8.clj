(ns clojure-solution.day8
  (:require [clojure.java.io :as io]
            [clojure.string :as string]))

(def lines (->> "day8.txt"
                io/resource
                io/reader
                line-seq))

(defn parse [line]
  (->> line
       (#(string/split % #"\s*\|\s*"))
       (map #(string/split % #"\s"))))


(defn solve1 [lines]
  (->> lines
       (map parse)
       (map second)
       flatten
       (map count)
       (filter #(or (= 2 %)
                    (= 7 %)
                    (= 4 %)
                    (= 3 %)))
       count))

(defn assign-unique [signal-patterns]
  (reduce (fn [acc [length digit]]
            (assoc acc
                   (first (filter #(= length (count %))
                                  signal-pattern))
                   digit))
          {}
          (map vector [2 4 3 7][1 4 7 8])))

;TODO: Naming!!!!!
(defn make-contain-f [contain-f]
  (fn [patterns contained number-pattern-map assign]
  (let [contained (number-pattern-map contained)]
    (loop [patterns patterns
         fully-contains nil
         contains-not []]
    (if (empty? patterns)
      [contains-not (assoc number-pattern-map assign fully-contains)]
      (let [f (first patterns)
            r (rest patterns)]
        (if (contain-f f contained)
          (recur r f contains-not)
          (recur r fully-contains (conj contains-not f)))))))))

(defn contains-pattern? [superset subset] (every? #(contains? (set superset) %) subset))

(def extract-fully-contains (make-contain-f #(contains-pattern? %1 %2)))
(def extract-is-contained (make-contain-f #(contains-pattern? %2 %1)))

(defn deduce-pattern [signal-patterns]
  (let [digit-pattern-map (reduce-kv #(assoc %1 %3 %2)
                                      {}
                                      (assign-unique signal-patterns))
        get-pattern-with-length (fn [n] (filter #(= n (count %)) signal-patterns))
        pattern-len-5 (get-pattern-with-length 5)
        pattern-len-6 (get-pattern-with-length 6)
        [pattern-len-5 digit-pattern-map] (extract-fully-contains pattern-len-5  1 digit-pattern-map 3)
        [pattern-len-6 digit-pattern-map] (extract-fully-contains pattern-len-6 3 digit-pattern-map 9)
        [pattern-len-5 digit-pattern-map] (extract-is-contained pattern-len-5 9 digit-pattern-map 5)
        [pattern-len-6 digit-pattern-map] (extract-fully-contains pattern-len-6 5 digit-pattern-map 6)
        digit-pattern-map (assoc digit-pattern-map 0 (first pattern-len-6))
        digit-pattern-map (assoc digit-pattern-map 2 (first pattern-len-5))
        pattern-digit-map (reduce-kv #(assoc %1 (sort %3) %2) {} digit-pattern-map)]

    pattern-digit-map))

(defn decode [line]
  (let [[signal-patterns digits] (parse line)
        pattern-digit-map (deduce-pattern signal-patterns)]
    (apply + (map * [1000 100 10 1]
                  (map #(pattern-digit-map (sort %)) digits)))))

(defn solve2 [lines]
  (->> lines
       (map decode)
       (apply +)))
