(ns clojure-solution.day10
  (:require [clojure-solution.aoc :as aoc]))


(def lines (aoc/read-lines "day10.txt"))

(def opening-chars [\{ \( \[ \< ])
(def matching-characters {\{ \} \[ \] \( \) \< \>})
(def points {\) 3 \] 57 \} 1197 \> 25137})
(def points2 {\) 1 \] 2 \} 3 \> 4})

(defn corrupted? [line]
  (loop [stack '() chars line]
    (if (not-empty chars)
      (let [[f & r] chars]
        (if (some #(= f %) opening-chars)
          (recur (cons f stack) r)
          (if (and (not-empty stack)
                   (= f (matching-characters (first stack))))
            (recur (rest stack) r)
            f)))
      stack)))


(defn solve1 [lines]
  (->> lines
       (map corrupted?)
       (filter char?)
       (map #(get points %))
       (apply +)))


(defn complete [stack]
  (map #(matching-characters %) stack))

(defn calc-score [missing]
  (reduce (fn [acc c]
            (+ (* acc 5) (points2 c)))
          0
          missing))

(defn solve2 [lines]
  (->> lines
       (map corrupted?)
       (filter seq?)
       (map complete)
       (map calc-score)
       (sort)
       (#(nth % (int (/ (count %) 2))))))
