(ns clojure-solution.day1
  (:require
   [clojure.java.io :as io]))

(def input (slurp (io/resource "day1.txt")))


(defn parse [input]
  (read-string (str "[" input "]")))


(defn solve [measurement]
  (->> measurement
       (#(map < % (rest %)))
       (filter identity)
       count))



#_(->> input
       parse
       solve)


#_(->> input
       parse
       (#(map + % (rest %) (rest (rest %))))
       solve)
