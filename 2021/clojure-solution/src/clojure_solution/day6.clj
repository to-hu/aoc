(ns clojure-solution.day6
  (:require [clojure.java.io :as io]))


(def initial-state (read-string (str "[" (slurp (io/resource "day6.txt"))"]")))


; TODO: analyze complextity
; model one fish and parallize the inital fishes
(defn solve1[initial-state days]
  (->> (range days)
       (reduce (fn [fishes day]
                 (let [fish-updated-timer (mapv #(if (= 0 %) 6 (dec %)) fishes)
                       new-fishes (repeat (count (filter (partial = 0)
                                                         fishes))
                                          8)]
                   (into fish-updated-timer new-fishes)))
               initial-state)
       ;; count
       ))

; NOTE: no improvement over singlethreaded solve1, why?
(defn solve2 [initial-state days]
  (let [num-threads 6
        size-per-thread (int (/ (count initial-state) num-threads))]
    (->> initial-state
         (partition-all size-per-thread)
         (map #(future (solve1 % days)))
         (map deref)
         (apply +))))

(defn get-steps-needed [days depth]

  (loop [times 0 depth depth]
    (if (>= depth days)
      times
      (recur (inc times) (* 2 depth)))))

(defn next-gen [fd _]
  (reduce (fn [acc n]
            (assoc acc n (flatten (map #(get fd %) (get fd n)))))
          fd
          (range 0 9)))

; erst 2 dann 4 dann 8 -> 16 usw
; TODO: vector instead of hashmaps

(defn solve3[initial-state days]
  (let [initial-depth 4
        steps-needed (get-steps-needed days initial-depth)
        fish-development (zipmap (range)
                                 (map #(solve1 [%] initial-depth)
                                      (range 0 9)))
        fish-develoment-complete (reduce next-gen fish-development (range steps-needed))
        fish-develoment-counts (reduce-kv #(assoc %1 %2 (count %3)) {} fish-develoment-complete)]
    ((flatten (map #(get fish-develoment-complete %) initial-state)))))

(defn make-evo [days]
  (let [initial-depth 4
        steps-needed (get-steps-needed days initial-depth)
        fish-development (zipmap (range)
                                 (mapv #(solve1 [%] initial-depth)
                                      (range 0 9)))
        fish-develoment-complete (reduce next-gen fish-development (range steps-needed))]
    (fn [start-age](vec (get fish-develoment-complete start-age)))))

(def evo-128 (make-evo 128))

;; (defn solve4 [initial-state days]
;;   (let [initial-depth 4
;;         steps-needed (get-steps-needed days initial-depth)
;;         fish-dev (map #(solve1 [%] initial-depth) (range 0 9))
;;         fish-dev-final (reduce next-gen-vec fish-dev (range steps-needed))
;;         fish-dev-counts (map count fish-dev-final)]
;;     (flatten (map #(fish-)))))
;;
;;
(defn evo-256-days [initial-state]
  (let [startages-after-128-days (mapv evo-128 [0 1 2 3 4 5 6 7 8])
        startages-count-after-128-days (mapv count startages-after-128-days)]
    (loop [acc 0
           colony (get startages-after-128-days (peek initial-state))
           initial-state (pop initial-state)]
      (cond (and (empty? initial-state)
                 (empty? colony))
            acc
            (and (empty? colony)
                 (not (empty? initial-state)))
            (recur acc (get startages-after-128-days (peek initial-state)) (pop initial-state))
            :else (recur (+' acc (get startages-count-after-128-days (peek colony)))
                         (pop colony)
                         initial-state)))))

#_(time (solve1 [1] 64))



#_(time (count (solve1 initial-state 128)))
#_(time (count (solve3 initial-state 128)))
#_(def erg (count (solve3 [0] 128)))
#_(time (solve3 [0] 256))
#_(frequencies initial-state)
#_(solve2 initial-state 256)
#_(evo-256-days initial-state)

initial-state
