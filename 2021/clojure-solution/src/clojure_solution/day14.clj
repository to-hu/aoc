(ns clojure-solution.day14
  (:require [clojure-solution.aoc :as aoc]
            [clojure.string :as string]))

(def lines (aoc/read-lines "day14.txt"))


(defn ->template [lines]
  (mapv (comp keyword str) (first lines)))

(defn ->pair-insertion-rules [lines]
  (->> lines
       (drop 2)
       (reduce (fn [acc line]
                 (let [[_ k1 k2 v] (re-matches #"(.)(.) -> (.)" line)]
                   (update-in acc [(keyword k1) (keyword k2)] (fn [_] (keyword v)))))
               {})))

(defn interleave-all[c1 c2]
  (conj (vec (interleave c1 c2)) (last c1)))

(defn step[insertion-rules polymer]
  (let [new-elements(reduce (fn [acc ks]
                              (conj acc (get-in insertion-rules ks)))
                            []
                            (map vector
                                 polymer
                                 (rest polymer)))]
    (interleave-all polymer new-elements)))


(defn solve1[lines]
  (let [polymer (->template lines)
        insertion-rules (->pair-insertion-rules lines)
        final-polymer (reduce (fn [acc _] (step insertion-rules acc))
                              polymer
                              (range 40))
        freq-values (vals (frequencies final-polymer))]
    (- (apply max freq-values)
       (apply min freq-values))))
