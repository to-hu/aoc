(ns clojure-solution.day16
  (:require [clojure-solution.aoc :as aoc]))

(def lines (aoc/read-lines "day16.txt"))

(def hex-bin-m {\0 [0 0 0 0]
                \1 [0 0 0 1]
                \2 [0 0 1 0]
                \3 [0 0 1 1]
                \4 [0 1 0 0]
                \5 [0 1 0 1]
                \6 [0 1 1 0]
                \7 [0 1 1 1]
                \8 [1 0 0 0]
                \9 [1 0 0 1]
                \A [1 0 1 0]
                \B [1 0 1 1]
                \C [1 1 0 0]
                \D [1 1 0 1]
                \E [1 1 1 0]
                \F [1 1 1 1]})

(defn ->bitstream [lines]
  (->> lines
       first
       (apply list)
       (map hex-bin-m)
       flatten))


(defn read-bits [stream n]
  [(take n stream) (drop n stream)])


(defn read-header [stream]
  (let [[version stream] (read-bits stream 3)
        [typ stream] (read-bits stream 3)]
    [(mapv aoc/bin->dec [version typ]) stream]))


(defn msb= [n bits]
  (if (= (first bits) n)
    bits
    false))

(defn read-literal-value [stream]
  (loop [acc []
         s stream]
    (let [[[msb & d] s] (read-bits s 5)]
      (if (= 0 msb)
        [(aoc/bin->dec (into acc d)) s]
        (recur (into acc d) s)))))

;; (declare parse)

(defn read-operator-value [stream]
  (let [[[length-typ] stream] (read-bits stream 1)]
    (if (= length-typ 0)
      (let [[length stream] (read-bits stream 15)
            length (aoc/bin->dec length)]))))

(defn parse [stream]
  (loop [acc []
         stream stream]
    (if (or (empty? stream)
            (every? zero? stream))
      acc
      (let [[version-bits stream] (read-bits stream 3)
            version (aoc/bin->dec version-bits)
            [typ-bits stream] (read-bits stream 3)
            typ (aoc/bin->dec typ-bits)
            [data stream](condp = typ
                           4 (read-literal-value stream))]
        (recur (conj acc [version typ data])
               stream)))))
