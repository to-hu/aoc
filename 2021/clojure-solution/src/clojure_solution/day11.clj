(ns clojure-solution.day11
  (:require [clojure-solution.aoc :as aoc]))

(def lines (aoc/read-lines "day11.txt"))

(defn parse [line]
  (mapv #(- (int %) 48) line))

(defn get-coordinates [matrix]
  (let [x-max (count (first matrix))]
    (for [x (range x-max)
          y (range (count matrix))]
      [x y])))

(defn add-coordinates [grid]
  (let [x-max (count (first grid))
        coordinates (get-coordinates grid)]
    (partition x-max (mapv vector (flatten grid)
          coordinates))))

(defn rise [level]
  (if (= level -1)
    -1
    (inc level)))

(def kernel [rise rise rise rise (fn [_] -1) rise rise rise rise])

(defn apply-kernel [matrix kernel [x y :as coordinates]]
  (reduce (fn [acc  [f coordinates]]
            (update-in acc coordinates f))
          matrix
          (filter #(identity (second %))
                  (map vector kernel
                       (aoc/get-kernel-coordinates matrix kernel coordinates)))))

(defn map-matrix [f matrix]
  (reduce (fn [acc coordinates]
            (update-in acc coordinates f))
          matrix
          (get-coordinates matrix)))

(defn get-flashing-coordinates [matrix]
  (->> matrix
       add-coordinates
       (apply concat)
       (filter (fn [[e-level]] (> e-level 9)))
       (map second)))

(defn count-flashing [matrix]
  (->> matrix
       flatten
       (filter #(= -1 %))
       count))

(defn step
  ([matrix] (step matrix kernel))
  ([matrix kernel]
   (loop [m (map-matrix inc matrix)]
     (let [flashing (get-flashing-coordinates m)]
       (if (empty? flashing)
         [(count-flashing m) (map-matrix #(if (= % -1) 0 %) m) ]
         (recur (reduce #(apply-kernel %1 kernel %2)
                        m
                        flashing)))))))

(defn solve1 [matrix]
  (first (reduce (fn [[sum m] _]
            (let [[f new-acc] (step m)]
              [(+ sum f) new-acc]))
          [0 matrix]
          (range 100))))

(defn solve2 [matrix]
  (let [all (* (count matrix) (count (first matrix)))]
    (loop [n 1
         m matrix]
      (let [[f m] (step m)]
        (if (= all f)
          n
          (recur (inc n) m))))))

#_(->> lines
       (mapv parse)
       solve1)

#_(->> lines
       (mapv parse)
       solve2)
