(ns clojure-solution.day9
  (:require [clojure-solution.aoc :as aoc]))

(def lines (aoc/read-lines "day9.txt"))

(defn parse [line]
  (mapv #(- (int %) 48) line))

(defn ->height-map [lines]
  (apply mapv vector (mapv parse lines)))


(defn ignore [_ & rest]
  false)


(def kernel-part1 [ignore identity ignore
                   identity ignore identity
                   ignore identity ignore])



(defn low-point? [height-map [x y :as coordinate]]
  (let [v (get-in height-map coordinate)
        neighbours (aoc/apply-kernel height-map kernel-part1 coordinate)]
    (if (every? #(< v %) neighbours)
      [coordinate v]
      false)))

(defn get-low-points [height-map]
  (let [all-coordinates (aoc/all-coordinates-of height-map)]
    (->> all-coordinates
         (map #(low-point? height-map  %1))
         (filter identity))))

(defn solve1 [lines]
  (->> lines
       ->height-map
       get-low-points
       (map second)
       (map inc)
       (apply +)))

(defn get-neighbours [height-map coordinate]
  (let [coordinates (map #(%1 %2)
                         kernel-part1
                         (aoc/get-kernel-coordinates height-map
                                                     kernel-part1
                                                     coordinate))]
    (->> coordinates
         (filter identity)
         (map #(vector % (get-in height-map %))))))


(defn region-growth [height-map start]
  (letfn [(not-9-neighbours [coordinate]
            (->> coordinate
                 (get-neighbours height-map)
                 (filter (fn [[_ v]] (not= v 9)))
                 (map first)))]
    (loop [region #{start}
           to-check (not-9-neighbours start)]
      (if (empty? to-check)
        region
        (let [new-to-check (reduce #(into %1 %2) [] (map not-9-neighbours to-check))
              new-to-check-witout-region (reduce #(disj %1 %2) (set new-to-check) region)
              new-region (reduce #(conj %1 %2) region to-check)]
          (recur new-region new-to-check-witout-region))))))


(defn get-regions [height-map]
  (->> height-map
       get-low-points
       (map first)
       (map (partial region-growth height-map))))


(defn solve2 [lines]
  (->> lines
       ->height-map
       get-regions
       (map count)
       (sort >)
       (take 3)
       (apply *)))
