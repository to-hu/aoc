package main

import (
	"testing"
)

func TestDay1(t *testing.T){
	td := map[string]int{"1122": 3, "1111": 4, "1234": 0, "91212129": 9,
		"9513446799636685297929646689682997114316733445451534532359": 51 , Input(1): 1343}

	TestHelper(t, td, Day1)
}

func TestDay1Part2(t *testing.T){
	td := map[string]int{"1212": 6, "1221": 0, "123425": 4, "123123": 12, "12131415": 4,
		  Input(1): 1274}
	TestHelper(t, td, Day1Part2)
}
