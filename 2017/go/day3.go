package main

import (
	"strconv"
)

type Point struct {
	X int
	Y int
}

type Cursor struct {
	Direction Point
	Position  Point
}

func Day3Part1(input string) int {
	address, _ := strconv.Atoi(input)
	_, cursor := buildGrid(makeValueFuncPart1(), func(x int) bool { return x != address })
	return abs(cursor.Position.X) + abs(cursor.Position.Y)

}

func Day3Part2(input string) int {
	address, _ := strconv.Atoi(input)
	grid, cursor := buildGrid(valueFuncPart2, func(x int) bool { return x <= address })
	return grid[cursor.Position]
}

func valueFuncPart2(grid map[Point]int, cursor Cursor) int {
	result := 0
	directions := []Point{{1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0}, {-1, -1}, {0, -1}, {1, -1}}
	for _, d := range directions {
		result += grid[nextPoint(Cursor{Position: cursor.Position, Direction: d}).Position]
	}
	return result
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func op(f func() int) func() int {
	return func() int { return -f() }
}

func square(x int) int {
	return x * x
}

func makeValueFuncPart1() func(map[Point]int, Cursor) int {
	var counter = 1
	return func(map[Point]int, Cursor) int {
		counter += 1
		return counter
	}
}

func buildGrid(valueFunc func(map[Point]int, Cursor) int, abortFunc func(int) bool) (map[Point]int, Cursor) {
	grid := map[Point]int{{0, 0}: 1}
	cursor := Cursor{Direction: Point{X: 1, Y: 0}, Position: Point{0, 0}}
	for abortFunc(grid[cursor.Position]) {
		cursor = nextPoint(cursor)
		if isLeftNeighbourEmpty(grid, cursor) {
			cursor = turnLeft(cursor)
		}
		grid[cursor.Position] = valueFunc(grid, cursor)
	}
	return grid, cursor
}

func nextPoint(cursor Cursor) Cursor {
	cursor.Position.X += cursor.Direction.X
	cursor.Position.Y += cursor.Direction.Y
	return cursor
}

func isLeftNeighbourEmpty(grid map[Point]int, cursor Cursor) bool {
	return 0 == grid[nextPoint(turnLeft(cursor)).Position]
}

func turnLeft(cursor Cursor) Cursor {
	cursor.Direction = map[Point]Point{
		{X: 1, Y: 0}:  {X: 0, Y: 1},
		{X: 0, Y: 1}:  {X: -1, Y: 0},
		{X: -1, Y: 0}: {X: 0, Y: -1},
		{X: 0, Y: -1}: {X: 1, Y: 0}}[cursor.Direction]
	return cursor
}
