package main

import (
	"reflect"
	"strings"
)

func Day4Part1(input string) int {
	return countValidPassphrases(input, isValid)
}

func Day4Part2(input string) int {
	return countValidPassphrases(input, isValidPart2)
}

func countValidPassphrases(input string, isValidFunc func([]string) bool) int {
	passphrases := (parse(input))
	result := 0
	for _, passphrase := range passphrases {
		valid := isValidFunc(passphrase)
		if valid {
			result += 1
		}
	}
	return result
}

type Anagram map[rune]int

func isValidPart2(passphrase []string) bool {
	usedWords := []Anagram{}
	for _, word := range passphrase {
		anagram := Anagram{}
		for _, char := range word {
			anagram[char] += 1
		}
		for _, a := range usedWords {
			if reflect.DeepEqual(a, anagram) {
				return false
			}
		}
		usedWords = append(usedWords, anagram)
	}
	return true
}

func isValid(passphrase []string) bool {
	usedWords := map[string]int{}
	for _, word := range passphrase {
		if usedWords[word] == 0 {
			usedWords[word] = 1
		} else {
			return false
		}
	}
	return true
}

func parse(input string) [][]string {
	lines := strings.Split(input, "\n")
	result := [][]string{}
	for _, line := range lines {
		result = append(result, strings.Split(line, " "))
	}
	return result
}
