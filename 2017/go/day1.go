package main

func Day1(input string) int{
	distance := 1
	digits := inputToDigits(input)
	return sameDigitSum(digits, distance)
}


func sameDigitSum(digits []int, distance int) int {
	sum := 0
	for i, c := range digits {
		if digits[(i+distance)%len(digits)] == c {
			sum += c
		}
	}
	return sum
}

func inputToDigits(input string) []int {
	digits := []int{}
	for _, c := range input {
		digits = append(digits, int(c)-48)

	}
	return digits
}

func Day1Part2(input string) int{
	digits := inputToDigits(input)
	distance := len(digits) / 2
	return sameDigitSum(digits, distance)
}
