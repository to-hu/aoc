package main

import (
	"testing"
)

func TestDay3Part1(t *testing.T) {
	td := map[string]int{"1": 0, "12": 3, "23": 2, "26": 5, "1024": 31, "368078": 371}
	TestHelper(t, td, Day3Part1)
}

func TestDay3Part2(t *testing.T) {
	td := map[string]int{"1": 2, "2": 4, "4": 5, "5": 10, "17": 23, "747": 806, "368078": 369601}
	TestHelper(t, td, Day3Part2)
}
