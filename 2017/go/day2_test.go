package main

import "testing"

func TestDay2(t *testing.T){
	td := map[string]int{"5	1	9	5\n7	5	3\n2	4	6	8": 18, Input(2): 47136}
	TestHelper(t, td, Day2)
}
func TestDay2Part2(t *testing.T){
	td := map[string]int{"5	9	2	8\n9	4	7	3\n3	8	6	5": 9, Input(2): 250}
	TestHelper(t, td, Day2Part2)
}
