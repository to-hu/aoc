package main

import (
	"fmt"
	"os"
	"testing"
	"strings"
)

func Input(day int) string {
	data, err := os.ReadFile(fmt.Sprintf("../input/%d.txt", day))
	if err != nil {
		fmt.Print(err)
	}
	return strings.Trim(string(data), "\n ")
}

func Last[T any](a []T) T {
	return a[len(a)-1]
}

func TestHelper(t testing.TB, td map[string]int, f func (string) int){
	t.Helper()
	for input, want := range td {

		got := f(input)
		if got != want {
			t.Errorf("got %d, wantet %d for input %s", got, want, input)
		}
	}
}
