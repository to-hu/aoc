package main

import "testing"

func TestDay4Part1(t *testing.T) {
	td := map[string]int{"aa bb cc dd ee": 1, "aa bb cc dd aa": 0, "aa bb cc dd aaa": 1, Input(4): 386}
	TestHelper(t, td, Day4Part1)
}

func TestDay4Part2(t *testing.T) {
	td := map[string]int{"abcde fghij": 1, "abcde xyz ecdab": 0, "a ab abc abd abf abj": 1,
		"iiii oiii ooii oooi oooo": 1, "oiii ioii iioi iiio": 0, Input(4): 208}
	TestHelper(t, td, Day4Part2)
}
