package main

import (
	"fmt"
	"strconv"
	"strings"
)

func Day2(input string) int {
	spreadsheet := inputToSpreadsheet(input)

	return checksum(spreadsheet,func(min, max int)int{return max - min}, minMax)
}

func Day2Part2(input string) int {
	return checksum(inputToSpreadsheet(input),func (x,y int)int{return x / y}, evenDividables)
}

func inputToSpreadsheet(input string) [][]int {
	lines := strings.Split(input, "\n")
	spreadsheet := [][]int{}
	fmt.Println(Last(lines))

	for l1, line := range lines {
		row := []int{}
		for l2, cell := range strings.Split(strings.Trim(line, " \t"), "\t") {
			if cell == "" {
				fmt.Printf("In row %d/%d  cell %d/%d is empty", l1, len(lines), l2, len(cell))
			}
			value, err := strconv.Atoi(cell)
			if err != nil {
				fmt.Print(err)
			}
			row = append(row, value)
		}
		spreadsheet = append(spreadsheet, row)
	}
	return spreadsheet
}

func checksum(spreadsheet [][]int,operator func (int, int)int, f func([]int)(int, int)) int {
	checksum := 0
	for _, row := range spreadsheet {
		min, max := f(row)
		checksum += operator(min, max)
	}

	return checksum
}

func minMax(row []int)(int, int){
	min := row[0]
	max := min
	for _, value := range row[1:] {

		if value < min {
			min = value
		}

		if value > max {
			max = value
		}

	}
	return min, max
}

func evenDividables(row []int)(int, int){
	for i, dividend := range row {
		for k, divisor := range row {
			if k != i && dividend % divisor == 0 {
				return dividend, divisor
			}
		}

	}
	return -1, -1
}
