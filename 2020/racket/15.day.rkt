#lang racket
(require rackunit "util.rkt")

(define (last-spoken most-recently-spoken numbers)
  ;;; instead of appending numbers, the most recent get's to the front to better fit lists
  (let  ([position-in-numbers (for/first ([n (in-list numbers)]
                                          [i (in-naturals 1)]
                                          #:when (= most-recently-spoken n))
                                i)])
    (if position-in-numbers
        position-in-numbers
        0)))

(define (turn numbers)
  (let ([most-recently-spoken (first numbers)])
    (cons (last-spoken most-recently-spoken
                       (rest numbers))
          numbers)))

(define (solve-first numbers)
  (for/fold ([acc (reverse numbers)]
             #:result (first acc))
            ([_ (in-range (- 2020 (length numbers)))])
    (turn acc)))

(define (make-solver turn-count)
  (λ (starting-numbers)
    (for/fold ([ht (hash)]
               [last-spoken #f]
               #:result last-spoken)
              ([i (in-range (sub1 turn-count))])
      (if (< i (sub1 (length starting-numbers)))
          (values (hash-set ht (list-ref starting-numbers i) i)
                  (list-ref starting-numbers (add1 i)))
          (let* ([prev-index (if (hash-has-key? ht last-spoken)
                                 (hash-ref ht last-spoken)
                                 #f)]
                 [speak (if prev-index
                            (- i prev-index)
                            0)])
            (values (hash-set ht last-spoken i)
                    speak))))))

(define starting-numbers (map string->number  (string-split (first (input->lines 15)) ",")))
