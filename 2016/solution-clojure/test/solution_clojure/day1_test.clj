(ns solution-clojure.day1-test
  (:require [clojure.test :refer :all]
            [solution-clojure.day1 :refer :all]))

(deftest d1p1-test
  (testing "examples"
    (is (= 5 (d1p1 "R2, L3")))
    (is (= 2 (d1p1 "R2, R2, R2")))
    (is (= 12 (d1p1 "R5, L5, R5, R3")))))

(deftest d1p2-test
  (testing "examples"
    (is (= 4 (d1p2 "R8, R4, R4, R8")))))
