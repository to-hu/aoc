(ns solution-clojure.day4-test
  (:require [solution-clojure.day4 :as sut]
            [clojure.test :refer :all]))

(def test-lines ["aaaaa-bbb-z-y-x-123[abxyz]"
                 "a-b-c-d-e-f-g-h-987[abcde]"
                 "not-a-real-room-404[oarel]"
                 "totally-real-room-200[decoy]"])

(deftest part1
  (testing "examples"
    (is (= 1514 (sut/part1 test-lines)))))
