(ns solution-clojure.day2-test
  (:require [solution-clojure.day2 :as sut]
            [clojure.test :as t]))


(def instructions ['ULL
                   'RRDDD
                   'LURDL
                   'UUUUD])

(t/deftest part1
  (t/testing "example"
    (t/is (= '(1 9 8 5) (sut/part1 instructions)))))

(t/deftest part2
  (t/testing "example"
    (t/is (= '(5 13 11 3) (sut/part2 instructions)))))
