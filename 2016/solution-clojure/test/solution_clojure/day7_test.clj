(ns solution-clojure.day7-test
  (:require [solution-clojure.day7 :as sut]
            [clojure.test :refer :all]))

(deftest day7
  (testing "part1 examples"
    (is (= true (sut/tls? (sut/parse "abba[mnop]qrst"))))
    (is (= false (sut/tls? (sut/parse  "abcd[bddb]xyyx"))))
    (is (= false (sut/tls? (sut/parse  "aaaa[qwer]tyui"))))
    (is (= true (sut/tls? (sut/parse  "ioxxoj[asdfgh]zxcvbn")))))
  (testing "part2 examples"
    (is (= true (sut/ssl? (sut/parse "aba[bab]xyz"))))
    (is (= false (sut/ssl? (sut/parse "xyx[xyx]xyx"))))
    (is (= true (sut/ssl? (sut/parse "aaa[kek]eke"))))
    (is (= true (sut/ssl? (sut/parse "zazbz[bzb]cdb"))))))
