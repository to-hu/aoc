(ns solution-clojure.day9-test
  (:require [solution-clojure.day9 :as sut]
            [clojure.test :refer :all]))



(deftest decompressing
  (are [x y] (= y (count (sut/decompress x)))
    "ADVENT" 6
    "A(1x5)BC" 7
    "(3x3)XYZ" 9
    "A(2x2)BCD(2x2)EFG" 11
    "(6x1)(1x3)A" 6
    "(1x1)A(1x1)B" 2
    "X(8x2)(3x3)ABCY" 18))



(deftest remove-ingnored-markers
  (are [x y] (= y (->> x sut/get-marker sut/remove-ignored-markers (mapv :raw) count))
    "ADVENT" 0
    "(1x1)A(1x1)B" 2
    "A(1x5)BC" 1))
