(ns solution-clojure.day2
  (:require
   [clojure.java.io :as io]))

(def numpad {1 {\L 1 \R 2 \U 1 \D 4}
             2 {\L 1 \R 3 \U 2 \D 5}
             3 {\L 2 \R 3 \U 3 \D 6}
             4 {\L 4 \R 5 \U 1 \D 7}
             5 {\L 4 \R 6 \U 2 \D 8}
             6 {\L 5 \R 6 \U 3 \D 9}
             7 {\L 7 \R 8 \U 4 \D 7}
             8 {\L 7 \R 9 \U 5 \D 8}
             9 {\L 8 \R 9 \U 6 \D 9}})


(def numpad2 {1 {\L 1 \R 1 \U 1 \D 3}
              2 {\L 2 \R 3 \U 2 \D 6}
              3 {\L 2 \R 4 \U 1 \D 7}
              4 {\L 3 \R 4 \U 4 \D 8}
              5 {\L 5 \R 6 \U 5 \D 5}
              6 {\L 5 \R 7 \U 2 \D 10}
              7 {\L 6 \R 8 \U 3 \D 11}
              8 {\L 7 \R 9 \U 4 \D 12}
              9 {\L 8 \R 9 \U 9 \D 9}
              10 {\L 10 \R 11 \U 6 \D 10}
              11 {\L 10 \R 12 \U 7 \D 13}
              12 {\L 11 \R 12 \U 8 \D 12}
              13 {\L 13 \R 13 \U 11 \D 13}})


(def data (read-string (str "["(slurp (io/resource "day2.txt")) "]")))


(defn parse-data [d]
  (->> d
       (map name)
       (map (partial apply vector))))



(defn make-next-key [numpad]
  (fn [key instruction](->> instruction
       (reductions #((numpad %1) %2) key)
       last)))


(defn make-part [numpad]
  (let [next-key (make-next-key numpad)]
    (fn [instructions]
      (->> instructions
           parse-data
           (reductions #(next-key %1 %2) 5)
           rest))))


(def part1 (make-part numpad))

(def part2 (make-part numpad2))
