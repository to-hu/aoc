(ns solution-clojure.day8
  (:require
   [clojure.string :as s]
   [clojure.java.io :refer [resource reader]]))

(def lines (->> "day8.txt"
                resource
                reader
                line-seq))
(defn parse [line]
  (-> line
      (s/replace #"x" " ")
      (s/replace #"y?=|by" "")
      (s/replace #"rotate " "rotate-")
      ((partial str "(list ") ")")
      read-string))

(defn create-pixel
  ([width height init](vec (take height (repeat (vec (take width (repeat init)))))))
  ([width height] (create-pixel width height false)))

(defn create-screen
  ([width height init]
   {:width width
    :height height
    :pixels (create-pixel width height) })
  ([width height]
   (create-screen width height false)))

(def screen (create-screen 50 6))

(defn screen->lines [{pixels :pixels}]
  (let [printable-pixels (map (partial map #(if % "#" ".")) pixels)]
    (mapv (partial apply str) printable-pixels)))

(defn print-screen [screen]
  (for [l (screen->lines screen)]
    (println l)))

(defn turn-on [{pixel :pixels} x y]
  (let [row (pixel y)
        new-row (assoc row x true)
        new-pixel (assoc pixel y new-row)]
    (assoc screen :pixels new-pixel)))

(defn rect [screen a b]
  (let [indexes (for [x (range a)
                     y (range b)]
                 [x y])]
    (reduce (fn [screen [x y]] (turn-on screen x y)) screen indexes)))

(defn rotate [v n]
  (->> v
       repeat
       (apply concat)
       (drop (- (count v) n))
       (take (count v))
       vec))


(defn- rotate-pixels-row [pixels row-index n]
  (let [row-index (mod row-index (count pixels))
        row (nth pixels row-index)]
        (assoc pixels row-index (rotate row n))))

(defn rotate-row [{pixels :pixels :as screen} row-index n]
  (assoc screen :pixels (rotate-pixels-row pixels row-index n)))

(defn rotate-column [screen column-index n]
  (let [transpose #(apply (partial mapv vector) %)
        transposed (transpose (:pixels screen))
        rotated (:pixels (rotate-row (assoc screen :pixels transposed)
                                     column-index
                                     n))
        back-transposed (transpose rotated)]
    (assoc screen :pixels back-transposed)))

(defn lit-lights [{:keys [pixels]}]
  (->> pixels
       (map #(remove false? %))
       (map count)
       (reduce +)
       ))

(print-screen (let [instructions (map (comp eval parse) lines)]
   (reduce (fn [screen [f a b]] (println f a b)(f screen a b)) screen instructions)))
