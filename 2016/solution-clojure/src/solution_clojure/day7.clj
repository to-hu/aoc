(ns solution-clojure.day7
  (:require
   [clojure.java.io :as io]
   [clojure.set :refer [intersection]]))

(def lines (->> "day7.txt"
                io/resource
                io/reader
                line-seq))

(defn parse [line]
  (->>(read-string (str "[" line "]"))
      (map (fn [x]
         (if (vector? x)
           [(name (first x))]
           (name x))))
      (#(hash-map :inside (mapv first (filterv vector? %))
         :outside (filterv string? %)))))

(defn abba? [s]
  (loop [s (seq s)]
    (let [[a1 b1 b2 a2] s]
      (cond (empty? s) false
            (and (= a1 a2)
                 (= b1 b2)
                 (not (= a1 b1))) true
            :else (recur (rest s))))))


(defn pattern [f part ip7]
  (->> ip7
       part
       (map f)
       (filter identity)
       (apply concat)
       ))

(defn has-pattern? [f part ip7]
  (->> ip7
       part
       (filter f)
       count
       (#(>= % 1))))

(defn tls? [ip7]
  (and (has-pattern? abba? :outside ip7)
       (not (has-pattern? abba? :inside ip7))))


(defn part1 [lines]
  (->> lines
       (map parse)
       (filter tls?)
       count))


(defn aba? [s]
  (loop [s s
         acc []]
    (let [[a1 b a2] s]
      (cond (empty? s) (if (empty? acc) false acc)
            (and (= a1 a2)
                 (not (= a1 b))) (recur (rest s)(conj acc (str a1 b a2)))
            :else (recur (rest s) acc)))))


(defn aba->bab [aba]
  (let [[a b] aba]
    (str b a b)))


(defn ssl? [ip7]
  (let [aba (map aba->bab (pattern aba? :outside ip7))
        bab (pattern aba? :inside ip7)]
    (and (not (empty? aba))
         (not (empty? bab))
         (not (empty? (intersection (set aba) (set bab)))))))

(defn part2 [lines]
  (->> lines
       (map parse)
       (filter ssl?)
       count))
