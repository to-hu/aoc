(ns solution-clojure.day9
  (:require [clojure.java.io :as io]
            [clojure.string :as string]))

(def input (string/trim (slurp (io/resource "day9.txt"))))

(defn get-end-index [{:keys [len n-chars index]}]
  (+ index len n-chars))

(defn plain-marker [marker next-index]
  (let [end-index (get-end-index marker)]
    (if (not (= end-index
                next-index))
      {:raw "plain"
                     :len 0
                     :n-repeat 1
                     :n-chars (- next-index
                                 end-index)
                     :index end-index}
      nil)))

;; TODO add endmarker if last-index isn't equal to (dec (count s))
(defn get-marker[s]
  (let [markers (re-seq #"\((\d+)x(\d+)\)" s)
        markers (mapv (fn [[marker & extracted-data]]
                        (let [[subsequent repeat] (map #(Integer/parseInt %) extracted-data)]
                          {:raw marker :len (count marker) :n-chars subsequent :n-repeat repeat}))
                      markers)
        [_ reduced-markers](reduce (fn [[index acc] {:keys [raw len] :as all}]
                                     (let [marker-index (string/index-of s raw index)
                                           new-index (+ marker-index len)]
                                       [new-index (conj acc (assoc all :index marker-index))]))
                                   [0 []]
                                   markers)]
    (if-let[p (plain-marker (last reduced-markers) (count s))]
      (conj reduced-markers p)
      reduced-markers)))

(defn reduce-first [f f-val coll]
  (if-let [coll (not-empty coll)]
    (let [fst (first coll)
          rst (rest coll)
          [_ result] (reduce f
                             [(f-val fst) [fst]]
                             rst)]
      result)
    []))

(defn remove-ignored-markers [markers]
  (reduce-first (fn [[last-marker-range acc]{:keys [len n-chars index] :as all}]
                  (println index)
                  (if (< index last-marker-range)
                    [last-marker-range acc]
                    [(get-end-index all) (conj acc all)]))
                get-end-index
                markers))

(defn add-plain-markers [markers]
  (reduce-first (fn []) identity markers))

(defn decompressed-length [markers]
  )

(defn expand-marker [s marker]
  (reduce (fn [[meta-data acc] [marker & subm]]
            (let [[last-marker-end index] (last meta-data)
                  [char-count repeat-count] (map #(Integer/parseInt %) subm)
                  marker-index (string/index-of s marker last-marker-end)]
              (if (not (< marker-index index))
                (let [no-marker-data (subs s index marker-index)
                      after-marker-index (+ marker-index
                                            (count marker))
                      subsequence (subs s
                                        after-marker-index
                                        (+ after-marker-index char-count))
                      new-index (+ after-marker-index
                                   (count subsequence))]
                  [(conj meta-data [after-marker-index new-index subsequence]) (into acc
                                                                                     (cons no-marker-data
                                                                                           (repeat repeat-count subsequence)))])
                [meta-data acc])))
          [[[0 0 ""]] []]
          marker))

(defn decompress [s]
  (let [marker (re-seq #"\((\d+)x(\d+)\)" s)
        [meta-data decompressed](expand-marker s marker)]
    (let [[_ last-index] (last meta-data)](if (< last-index (count s))
                                            (conj decompressed (subs s last-index))
                                            decompressed))))

#_(-> input
      get-marker
      ;; remove-ignored-markers

      )


(decompress "ADVENT")
(decompress "A(2x2)BCD(2x2)EFG")
(decompress "(1x1)A(1x1)B")
