(ns solution-clojure.utils)


(def input-dir "~/project/aoc/2016/input")

(defn input [day part]
  (let [input-file-format-str "/home/tobi/projects/aoc/2016/input/%02dday%d.txt"]
    (slurp (format input-file-format-str day part))))

(defn abs [n] (max n (- n)))
