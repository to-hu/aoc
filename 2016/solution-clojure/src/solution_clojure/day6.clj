(ns solution-clojure.day6
  (:require
   [clojure.java.io :as io]
   [clojure.string :as s]))

(def lines (line-seq (io/reader (io/resource "day6.txt"))))


(defn make-part [f]
  (fn [lines]
  (->> lines
       (apply (partial map vector))
       (map frequencies)
       (map #(sort-by (fn [[x y]] (f y)) %))
       (map ffirst)
       (apply str))))


(def part1 (make-part -))

(def part2 (make-part +))
