(ns solution-clojure.day5
  (:require
   [clojure.string :as s]))

(def input "cxdnnyjw")

(defn md5 [s]
  (apply str
         (map (partial format "%02x")
              (.digest (doto (java.security.MessageDigest/getInstance "MD5")
                         .reset
                         (.update (.getBytes s)))))))


(defn valid-index? [door-id i]
  (let [hash (md5 (str door-id i))]
    (if (= (subs hash 0 5)
         "00000")
      hash
      false)))


(defn find-valid-hashes [door-id]
  (letfn [(calc [door-id step i]
            (if-let [hash (valid-index? door-id i)]
              [i hash]
              (recur door-id step (+ i step))))]
    (->> (range 1 9)
         (map #(future (calc door-id 8 %)))
         (map #(deref %))
         (sort-by (fn [[x]] x)))))



(defn part1 [valid-hashes]
  (->> valid-hashes
       (map second)
       (map #(nth % 5))))


(defn part2 [valid-hashes]
  (let [keys (concat (map (comp first str) (range 1 10)) '(\a \b \c \e \f))
        values (take 15 (drop 6 (range)))
        m (into {} (map vector keys values))]
    (map #(nth % (m (nth % 6))) valid-hashes))
