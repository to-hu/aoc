(ns solution-clojure.day3
  (:require
   [clojure.java.io :as io]
   [clojure.string :as s]))

(def data (butlast (read-string (str "[[" (s/replace (slurp (io/resource "day3.txt")) #"\n" "][") "]]"))))


(defn triangle? [[a b c]]
  (and (> (+ a b) c)
       (> (+ b c) a)
       (> (+ c a) b)))


(defn part1 [data]
  (->> data
       (filter triangle?)
       count))


(defn part2 [data]
  (let [columns [(map first data)
                 (map second data)
                 (map #(nth % 2) data)]]
    (->> columns
         (mapcat (partial partition 3))
         (filter triangle?)
         count)))
