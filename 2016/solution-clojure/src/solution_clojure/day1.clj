(ns solution-clojure.day1
  (:require [clojure.string :as string])
  (:require [solution-clojure.utils :as aoc]))

(defn decode-input [raw-input]
  (let [input (map string/trim (string/split raw-input #","))]
    (for [i input]
      [(if (= (first i) \L)
         -1
         1)
       (read-string (string/join (rest i)))])))

(defn gen-locations [decoded-instructions]
  (let [dirs [[0 1] [1 0] [0 -1] [-1 0]]]
    (->> decoded-instructions
         (reduce  (fn [[index-1 locations][index-change distance]]
                    (let [index (mod (+ index-1 index-change) (count dirs))
                          coordinates-1 (last locations)
                          coordinates (for [d (range 1 (inc distance))]
                                        (vec (map + coordinates-1 (map (partial * d) (dirs index)))))]
                      [index (apply (partial conj locations) coordinates)])
                    ) [0 [[0 0]]])
         second)))


(defn d1p1 [raw-input]
    (->> raw-input
         decode-input
        gen-locations
        last
        (map aoc/abs)
        (apply +)))


(defn d1p2 [raw-input]
  (->> raw-input
       decode-input
       gen-locations
       (reduce (fn [[acc seen] location]
                 (if (and (seen location)
                          (not acc)) [location seen]
                     [acc (conj seen location)]))
               [nil #{}])
       first
       (map aoc/abs)
       (apply +)))
