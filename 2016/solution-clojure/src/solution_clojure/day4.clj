(ns solution-clojure.day4
  (:require
   [clojure.java.io :as io]
   [clojure.string :as s]))


(def lines (line-seq (io/reader (io/resource "day4.txt"))))


(defn parse [line]
  (let [room-info (read-string (str "["(s/replace line #"[-\[]" " ")))
        room-hash (name (last room-info))
        room-name-enc (map name (butlast (butlast room-info)))
        room-id (nth room-info (- (count room-info) 2))]
    {:name room-name-enc :id room-id :hash room-hash}))


(defn sort-dist [distribution]
  "first compare most common than alphabetization"
  (into (sorted-map-by (fn [x y]
                                        ; reverse order of x and y to accomplish
                                        ; sorting from most common to least common
                         (let [c (compare (get distribution y)
                                          (get distribution x))]
                           (if (not= 0 c)
                             c
                             (let [c (compare x y)]c)))))
        distribution))


(defn valid? [room]
  (= (->> room
          :name
          (apply str)
          frequencies
          sort-dist
          keys
          (take 5))
     (apply list (:hash room))))


(defn part1 [lines]
  (->> lines
       (map parse)
       (filter valid?)
       (map :id)
       (apply +)))


(defn shift-char [key c]
  (-> c
      int
      (- 97)
      (+ key)
      (mod 26)
      (+ 97)
      char))

(defn ceasar [key s]
  (->> s
       (apply list)
       (map (partial shift-char key))
       (apply str)))

(defn decrypt [room]
  (assoc room :decrypted (map (partial ceasar (:id room)) (:name room))))

(defn part2 [lines]
  (->> lines
       (map parse)
       (filter valid?)
       (map decrypt)
       (filter #(= (:decrypted %)
                   '("northpole" "object" "storage")))
       first
       :id))
