#lang racket
(require rackunit "util.rkt")

(define (reduce-escaped-chars str)
  (let* ([inner (substring str 1 (- (string-length str) 1))]
         [hex-pattern #rx"\\\\(x[0-9abcdef][0-9abcdef]|\\\\|\")"]
         [removed-hex (regexp-replace* hex-pattern inner "x")])
    removed-hex))

(define (string->chars-repr str)
  (string-length (reduce-escaped-chars str)))

(module+ test
  (check-equal? (string->chars-repr "\"\"") 0)
  (check-equal? (string->chars-repr "\"\"\"") 1)
  (check-equal? (string->chars-repr "\"\\x27\"") 1)
  (check-equal? (string->chars-repr "\"\\x27\\x27\"") 2)
  (check-equal? (string->chars-repr "\"abc\"")3))



(define input (get-input file->lines 8 "first"))
(define t (get-input file->lines 8 "test"))

(define (solve-first lines)
  (apply + (map -
                (map string-length lines)
                (map string->chars-repr lines))))

(module+ test
  (check-equal? (solve-first (get-input file->lines 8 "test")) 12))

(define (string->chars-new-repr str)
  (let* ([pattern #rx"\\\\"]
        [backslash-matches (length (regexp-match* pattern str))]
        [doublequotes-matches (length (regexp-match* #rx"\"" str))]
        [original-length (string-length str)])
    (+ 2 original-length doublequotes-matches backslash-matches)))


(define (solve-second lines)
  (- (apply + (map string->chars-new-repr lines))
     (apply + (map string-length lines))))

(module+ test
  (check-equal? (solve-second (get-input file->lines 8 "test")) 19))
