#lang racket
(require rackunit)

(define (straight? password)
  (let* ([num-list (map char->integer (string->list password))]
         [num-list-rest (rest num-list)]
         [num-list-rest-rest (rest num-list-rest)])
    (for/first ([i num-list]
                [i+1 num-list-rest]
                [i+2 num-list-rest-rest]
                #:when (and (= (- i+1 i) 1)
                            (= (- i+2 i+1) 1)))
      #t)))

(define (two-pairs? password)
  (> (length (regexp-match* #px"(.)\\1" password)) 1))

(define (contains-confusing-letters? password)
  (regexp-match? #rx"[lio]" password))

(define (meet-requirements? password)
  (and (straight? password)
       (two-pairs? password)
       (not (contains-confusing-letters? password))))


(define (next-password password)
  (let* ([reverse-num-list (map char->integer (reverse (string->list password)))]
        [z-as-int 122]
        [a-as-int 97]
        [wraps? (λ (n) (> n z-as-int))]
        [char-add1 (λ (n) (let ([n+1 (add1 n)])
                            (if (wraps? n+1)
                                a-as-int
                                n+1)))])
    (for/fold ([acc '()]
               [previous-wrapped #t]
               #:result (apply string (map integer->char acc)))
              ([i reverse-num-list])
      (let ([i+1 (add1 i)])
        (if previous-wrapped
            (values (cons (char-add1 i) acc) (wraps? i+1))
            (values (cons i acc) #f))))))

(define (next-valid-password password)
  (for/fold ([acc password])
            ([_ (in-naturals)]
             #:break (and (meet-requirements? acc)
                          (not (string=? acc password))))
    (next-password acc)))
;; hijklmmn meets the first requirement (because it contains the straight hij) but fails the second requirement requirement (because it contains i and l).
;; abbceffg meets the third requirement (because it repeats bb and ff) but fails the first requirement.
;; abbcegjk fails the third requirement, because it only has one double letter (bb).
;; The next password after abcdefgh is abcdffaa.
;; The next password after ghijklmn is ghjaabcc

(module+ test
  (check-equal? (straight? "hijklmmn") #t)
  (check-equal? (two-pairs? "hijklmmn") #f)
  (check-equal? (contains-confusing-letters? "hijklmmn") #t)
  (check-equal? (meet-requirements? "hijklmmn" ) #f)
  (check-equal? (straight? "abbceffg") #f)
  (check-equal? (two-pairs? "abbceffg") #t)
  (check-equal? (contains-confusing-letters? "abbceffg") #f)
  (check-equal? (meet-requirements? "abbceffg") #f)
  (check-equal? (two-pairs? "abbcegjk") #f)
  (check-equal? (meet-requirements? "abcdffaa")  #t)
  (check-equal? (meet-requirements? "ghjaabcc")  #t)
  (check-equal? (next-valid-password "abcdefgh")  "abcdffaa")
  (check-equal? (next-valid-password "ghijklmn") "ghjaabcc")
  )
