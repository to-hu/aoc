#lang racket
(require rackunit)
(define input "1113222113")

(define (string->num-list x)
  (map (compose string->number string) (string->list x)))
(define (num-list->string x)
  (apply string-append (map number->string x)))

(define (look-and-say input)
  (define (acc->num-list acc)
    ((compose num-list->string flatten)
     (map (λ (x) (list (length x) (car x))) acc)))
  (let ([input (string->num-list input)])
    (for/fold ([acc '()]
               [subacc '()]
               #:result (acc->num-list (reverse (if (null? subacc)
                                                    acc
                                                    (cons subacc acc))))
               )
              ([i input])
      (cond     [(or (null? subacc)
                     (= i (first subacc))) (values  acc  (cons i subacc))]
                [else (values (cons subacc acc) (cons i '()))]))))

(define (make-solver n)
  (define (solver input)
  (let ([result (for/fold ([acc input])
            ([_ (in-range 0 n)])
                  (look-and-say acc))])
    (string-length result)))
  solver)

(define solve-first (make-solver 40))
(define solve-second (make-solver 50))
(module+ test
 (check-equal? (look-and-say "1") "11")
 (check-equal? (look-and-say "11") "21")
 (check-equal? (look-and-say "21") "1211")
 (check-equal? (look-and-say "1211") "111221")
 (check-equal? (look-and-say "111221") "312211"))
