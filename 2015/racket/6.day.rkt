#lang racket
(require "util.rkt" racket/bool rackunit)


(define (grid from to)
  (match-define (list x1 y1) from)
  (match-define (list x2 y2) to)
  (let* ([get-indexes (λ (js) (let ([y (modulo js 1000)])
                                (list (/ (- js y) 1000) y)))]
         [proc (λ (js)
                 (match-define (list x y) (get-indexes js))
                 (and (>= x x1)
                      (>= y y1)
                      (<= x x2)
                      (<= y y2)))])
    (build-vector (* 1000 1000) proc)))


(define (make-string->instruction toggle turn-on turn-off)
  (define (string->instruction str)
    (define pattern  #px"(toggle|turn on|turn off)\\s+(\\d+),(\\d+)\\s+through\\s+(\\d+),(\\d+)")
    (match-define (list m instruction x1 y1 x2 y2) (regexp-match pattern str))
    (define (get-f instruction) (cond [(string=? "toggle" instruction) toggle]
                                      [(string=? "turn on" instruction) turn-on]
                                      [(string=? "turn off" instruction) turn-off]))

    (list (get-f instruction) (grid (map string->number (list x1 y1)) (map string->number (list x2 y2)))))
  string->instruction)

(define string->instruction1 (make-string->instruction xor
                                                       (λ (x y) (or x y))
                                                       (λ (x y) (if y #f x))))

(define toggle2 (λ (x y) (if y (+ 2 x) x)))
(define turn-on2 (λ (x y) (if y (add1 x) x)))
(define turn-off2 (λ (x y) (if y (if (= x 0)
                                     0
                                     (sub1 x))
                               x)))
(define string->instruction2 (make-string->instruction toggle2 turn-on2 turn-off2))
(define (lit-lights grid)
  (vector-length (vector-filter (λ (x) x) grid)))

(define (make-solver string->instruction init-value resultf)
  (define (solve lines)
    (for/fold ([acc (make-vector (* 1000 1000) init-value)]
               #:result (resultf acc))
              ([line lines])
      (match-define (list combinator vec) (string->instruction line))
      (vector-map combinator acc vec)))
  solve)

(define (total-brightness grid)
  (apply + (vector->list grid)))

(define solve-first (make-solver string->instruction1 #f lit-lights))
(define solve-second (make-solver string->instruction2 0
                                  total-brightness
                                  ;; (λ (x) x)
                                  ))


(define lines (get-input file->lines 6 "first"))


(module+ test
  (check-equal? (solve-second '("turn on 0,0 through 0,8")) 9)
  ;; (check-equal? (solve-second '("turn on 0,0 through 0,4" "toggle 0,0 through 0,9")) 5)
  ;; (check-equal? (solve-second '("turn on 0,0 through 0,9" "turn off 0,5 through 0,9")) 5)
  ;; (check-equal? (solve-second '("turn on 0,0 through 0,9" "turn off 0,5 through 0,9" "toggle 0,0 through 0,9")) 5)
  ;; (check-equal? (solve-second '("turn on 0,0 through 0,8" "turn off 0,0 through 0,8")) 0)
  ;; (check-equal? (solve-second '("turn on 0,0 through 8,0" "toggle 0,0 through 999,0")) 991)
  ;; (check-equal? (solve-second '("turn on 0,0 through 999,499")) 500000)
  ;; (check-equal? (solve-second '("turn on 0,0 through 999,499" "toggle 0,0 through 999,999")) 500000)
  ;; (check-equal? (solve-second '("toggle 0,0 through 999,0")) 1000)
  )


(module+ test
  (check-equal? (solve-first '("turn on 0,0 through 0,8")) 9)
  (check-equal? (solve-first '("turn on 0,0 through 0,4" "toggle 0,0 through 0,9")) 5)
  (check-equal? (solve-first '("turn on 0,0 through 0,9" "turn off 0,5 through 0,9")) 5)
  (check-equal? (solve-first '("turn on 0,0 through 0,9" "turn off 0,5 through 0,9" "toggle 0,0 through 0,9")) 5)
  (check-equal? (solve-first '("turn on 0,0 through 0,8" "turn off 0,0 through 0,8")) 0)
  (check-equal? (solve-first '("turn on 0,0 through 8,0" "toggle 0,0 through 999,0")) 991)
  (check-equal? (solve-first '("turn on 0,0 through 999,499")) 500000)
  (check-equal? (solve-first '("turn on 0,0 through 999,499" "toggle 0,0 through 999,999")) 500000)
  (check-equal? (solve-first '("toggle 0,0 through 999,0")) 1000))
