(ns day1
  (:require [clojure.java.io :as io]
            [aoc]))
(defn calories []
  (->> (aoc/read-lines 2022 1)
     (map #(if (not= "" %) (Integer/parseInt %) %))
     (partition-by (partial not= ""))
     (remove (partial = '("")))
     (map (partial apply +))))

; part 1
#_(->> (calories)
       sort
       last)

; part 2
#_(->> (calories)
       (sort >)
       (take 3)
       (apply +))
