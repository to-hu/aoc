(ns day5
  (:require [aoc]))

(def test-input (list (list  "    [D]    "
                             "[N] [C]    "
                             "[Z] [M] [P]"
                             " 1   2   3 ")
                      (list "move 1 from 2 to 1"
                            "move 3 from 1 to 3"
                            "move 2 from 2 to 1"
                            "move 1 from 1 to 2")))

(defn parse-stacks [stack-lines]
  (->> stack-lines
       (map (partial partition-all 4))
       (map #(map second %))
       butlast
       (apply map list)
       (map (partial drop-while #(= \space %)))
       (zipmap (drop 1 (range)))))

(defn extract-numbers [line]
  (map #(Integer/parseInt %)(re-seq #"\d+" line)))

(defn parse-instruction [line order-f]
  (fn [stacks]
    (let [[c f t] (extract-numbers line)
          crates (take c (get stacks f))]
      (-> stacks
          (assoc f (drop c (get stacks f)))
          (assoc t (into (get stacks t) (order-f crates)))))))

(defn solve [f stacks instructions]
  (let [stacks (parse-stacks stacks)
        instructions (map #(parse-instruction % f) instructions)
        rearanged-stacks (->> instructions
                              (reduce #(%2 %1) stacks))]
    (map #(first (get rearanged-stacks %)) (range 1 10))))

#_ (->> (two-part-input 2022 5)
        (apply solve identity))

#_ (->> (two-part-input 2022 5)
        (apply solve reverse))
