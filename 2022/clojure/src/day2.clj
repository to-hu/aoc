(ns day2
  (:require [aoc]
            [clojure.string :as s]))

(def scores {"A X" 4 "A Y" 8 "A Z" 3
             "B X" 1 "B Y" 5 "B Z" 9
             "C X" 7 "C Y" 2 "C Z" 6})

#_(->> (aoc/read-lines 2022 2)
     (map (partial get scores))
     (apply +))
split-at

(def outcome {"X" 0 "Y" 3 "Z" 6})

(def rules {"A" {"X" 3 "Y" 1 "Z" 2}
            "B" {"X" 1 "Y" 2 "Z" 3}
            "C" {"X" 2 "Y" 3 "Z" 1}})


#_(->> (aoc/read-lines 2022 2)
     (map #(s/split % #" "))
     (map #(+ (get-in rules %) (get outcome (second %))))
     (apply +))
