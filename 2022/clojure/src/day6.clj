(ns day6
  (:require [aoc]))

(def test-data ["bvwbjplbgvbhsrlpgdmjqwftvncz"
                "nppdvjthqldpwncqszvftbrmjlhg"
                "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"
                "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"])


(defn make-solver [distinct-chars]
  (fn solve [input]
  (loop [input input
         n distinct-chars]
    (if (= distinct-chars (count (set (take distinct-chars input))))
      n
      (recur (drop 1 input) (inc n))))))


#_(map (make-solver 4) test-data)

#_((make-solver 4) (input-line 2022 6))

#_((make-solver 14) (input-line 2022 6))
