(ns day7
  (:require [aoc :as a]
            [clojure.test :as test]
            [clojure.string :as cs]))


(def input (a/read-lines 2022 7))

(defn take-ls-output [lines]
  (split-with #(not= (first %) \$) lines))

(defn ls-output->map [ls-output]
  (reduce (fn [acc line]
            (if (= \d (first line))
              (assoc acc (keyword (apply str (drop 4 line))) {})
              (let [[size name] (cs/split line #" " )]
                (assoc acc name (Integer/parseInt size)))))
          {}
          ls-output))

(defn butlastv [v]
  (let [c (count v)]
    (if (> c 0)
      (subvec v 0 (dec (count v)))
      [])))

(defn ->fs-tree [lines]
  (loop [lines lines ks [] m {}]
    (if (empty? lines)
      m
      (let [[[line] rest-lines] (split-at 1 lines)
            [_ command & args] (cs/split line #" ")]
        (condp = command
          "cd" (condp = (first args)
                   ".." (recur rest-lines (butlastv ks) m)
                   "/"  (recur rest-lines [:/] m)
                   :else (recur rest-lines (conj ks (keyword (first args))) m))
          "ls" (let [[output rest-lines] (take-ls-output rest-lines)]
                 (recur rest-lines ks (assoc-in m ks (ls-output->map output)))))))))

(defn contains-only-files? [dir]
  (every? string? (keys dir)))

(defn all-files [dir]
  (apply + (filter number? (vals dir))))

(defn get-dir-sizes [fs-tree path]
  (let [current-dir (get-in fs-tree path)
        k (last path)]
    (if (contains-only-files? current-dir)
      {path (all-files current-dir)}
      (let [subdirs-paths (map (partial conj path) (filter keyword? (keys current-dir)))
            new-sizes (apply merge (map #(get-dir-sizes fs-tree %) subdirs-paths))]
        (assoc new-sizes path (+ (all-files current-dir)
                                 (apply + (map #(get new-sizes %) subdirs-paths))))))))

(def test-input["$ cd /"
                "$ ls"
                "dir a"
                "14848514 b.txt"
                "8504156 c.dat"
                "dir d"
                "$ cd a"
                "$ ls"
                "dir e"
                "29116 f"
                "2557 g"
                "62596 h.lst"
                "$ cd e"
                "$ ls"
                "584 i"
                "$ cd .."
                "$ cd .."
                "$ cd d"
                "$ ls"
                "4060174 j"
                "8033020 d.log"
                "5626152 d.ext"
                "7214296 k"])

(def expected-parsed-test-input
  {:/ {:a {:e {"i" 584} "f" 29116 "g" 2557 "h.lst" 62596} :d {"j" 4060174 "d.log" 8033020 "d.ext" 5626152 "k" 7214296} "b.txt" 14848514 "c.dat" 8504156}})

(test/deftest part-1
  (test/testing
    (test/is (= (->fs-tree test-input) expected-parsed-test-input))
    (test/is (= (get-dir-sizes expected-parsed-test-input [:/]) {[:/] 48381165  [:/ :a] 94853 [:/ :a :e] 584 [:/ :d] 24933642}))))
