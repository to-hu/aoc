(ns day4
  (:require [aoc]
            [clojure.string :as s]))

(defn parse [line]
  (->> (s/split line #"[,-]")
       (map #(Integer/parseInt %))
       (split-at 2)
       ))

#_(->> (aoc/read-lines 2022 4)
       (map parse)
       (filter (fn [[[low1 up1][low2 up2]]]
                 (or (and (>= low1 low2)
                          (<= up1 up2))
                     (and (>= low2 low1)
                          (<= up2 up1)))))
       count)
