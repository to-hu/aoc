(ns day3
  (:require [aoc]
            [clojure.set :as cs]))

(defn char-range [start end]
  (map char (range (int start) (inc (int end)))))


(def priority (merge (zipmap (char-range \a \z)
                             (drop 1 (range)))
                     (zipmap (char-range \A \Z)
                             (drop 27 (range)))))

; part 1
#_(->> (aoc/read-lines 2022 3)
       (map #(split-at (/ (count %) 2) %))
       (map (fn [[x y]] (cs/intersection (set x) (set y))))
       (map first)
       (map (partial get priority))
       (apply +))

#_(->> (aoc/read-lines 2022 3)
       (map set)
       (partition 3)
       (map (partial apply cs/intersection))
       (map first)
       (map (partial get priority))
       (apply +))
